package com.iesportada.semipresencial.propertymeasurement;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Rafa Narvaiza
 * Here we have an activity designed to show a GoogleMap object and a few tools of measurement.
 * 1st we implement an action bar with three item display.
 * On this items, we'll be able to call some measurement tools, like area or perimeter measure.
 * We'll be able to use LongPress and ShortPress to put a marker on the map and start drawing a polyline as we shortpress.
 * Then we'll call the measurement functions that will close the polyline on a polygon and show a toast with the required info.
 */

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private PolygonOptions poly;
    private Polygon polygon;
    public static Polyline LINE;
    private List coordList;
    private GoogleMap mMap;
    private String message;
    //We initialize this two variables on declaration area to get ride along it on first click.
    private Double area = 0.0;
    private Double perimetro = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        getSupportActionBar().setTitle("Medidor en mapas");

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.area:
                getAreaMeasure();
                return true;
            case R.id.perimetro:
                getPeriemtroMeasure();
                return true;
            case R.id.borrar:
                clearPolygonInstance();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Function desinged to work on singleton mode. On this way we avoid multiple instances of markers, lines or polygons.
     */
    private void clearPolygonInstance() {
        mMap.clear();//1st, we erase all map markers.
        try{
            if(!LINE.getPoints().isEmpty()){ //Then we check if they are drawn lines.
                LINE.remove();
                // As we call this method from menu & any other polygon instance, we have to initialize the line on the longClickListener.
                setArea(0.0);
                setPerimetro(0.0);
            }
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Function to call computeLength from SphericalUtil to measure the length of the lines composing the polygon.
     */

    private void getPeriemtroMeasure() {
        closePolygon();
        setPerimetro(SphericalUtil.computeLength(getCoordList()));

        if(m2kmSplitter(getPerimetro())<4){
            message = "El perímetro del polígono es: " + getPerimetro() + " m";
        }
        else {

            message = "El perímetro del polígono es: " + getPerimetro()/1000 + " km";
        }

        showMessage(message, getApplicationContext());
    }


    /**
     * Function to call computeLength from SphericalUtil to measure the area inside the polygon.
     */
    private void getAreaMeasure() {
        closePolygon();
        setArea(SphericalUtil.computeArea(getCoordList()));

        if (!squareMeterSplitter(getArea())){
            message = "El area del polígono es: " + getArea() + " m2";
        }
        else{
            message = "El area del polígono es " + getArea() / 1000000 + " km2";
        }
        showMessage(message, getApplicationContext());
    }

    /**
     * Function to call the constructor to create a polygon with a given collection of LatLng. This LatLng list comes from a polyline and here we close the polyline to create a polygon.
     */

    private void closePolygon(){
        poly = new PolygonOptions();
        for(int i = 0; i < getCoordList().size(); i++){
            poly.addAll(getCoordList());
        }
        polygon = mMap.addPolygon(poly);
    }

    // Simple function to get a better readibility for huge polygons.
    private int m2kmSplitter(double number){
        String[] splitted = String.valueOf(number).split("\\.");
        return splitted[0].length();
    }

    private boolean squareMeterSplitter(double number){
        boolean isBigNumber = true;
        if(String.valueOf(number).indexOf("E") == -1){
            isBigNumber = false;
        }
        else {
            isBigNumber = true;
        }
        return isBigNumber;
    }

    /**
     * Function to hold map click listeners.
     * @param googleMap
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {

        setCoordList(new ArrayList<>());
        mMap = googleMap;

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                clearPolygonInstance(); //To avoid any unliked longpress, we erase previous drawing.
                LINE = googleMap.addPolyline(new PolylineOptions()
                        .clickable(true));
                if(getCoordList().size()>0){
                    getCoordList().clear(); //Ever any instance of the LatLng collection, we make sure its empty.
                }
                else
                {
                    getCoordList().add(latLng);
                    googleMap.addMarker(new MarkerOptions().position((LatLng) getCoordList().get(0)).title("origen" + getCoordList().get(0).toString()));
                    showMessage("Coordenadas " + getCoordList().get(0) + " agregadas correctamente", getApplicationContext());
                }

            }
        });
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                getCoordList().add(latLng);
                //To avoid any nullpointer on singleclick at beginning of activity lifecycle, we go through the collection emptyness exception.
                try{
                    //We will be able to draw lines on map if there ISN'T any polygon drawn. On this way, we avoid to draw new lines from last onClick interaction when a polyline is closed anc a polygon is created.
                    if (getPerimetro() == 0.0 && getArea() == 0.0){
                        LINE.setPoints(getCoordList());
                    }

                } catch (NullPointerException e){
                    System.out.println(e.getMessage());
                }
            }
        });
    }


    private void showMessage(String message, Context context){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public List getCoordList() {
        return coordList;
    }

    public void setCoordList(List coordList) {
        this.coordList = coordList;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Double getPerimetro() {
        return perimetro;
    }

    public void setPerimetro(Double perimetro) {
        this.perimetro = perimetro;
    }
}