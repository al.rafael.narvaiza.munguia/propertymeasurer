# TO U05 - HLC


## Maps

### Inicialización de la actividad de mapas.
Para comenzar en esta actividad, hemos creado un nuevo proyecto desde AS y hemos seleccionado la actividad de Mapas:

![map activity on new project](https://i.imgur.com/BNWtDRS.png)

En el archivo google_maps_api.xml hemos añadido nuestra API de google cloud, con los sdk maps previamente activados.

### Diseño de la actividad.
Los proyectos de mapas en AS extienden de FragmentActivity, sin embargo, en nuestro caso, no vamos a necesitarlo. Además, esta herencia no nos va a facilitar la implementación de una barra de soporte. Para conseguir habilitar esta barra de soporte, cambiaremos la herencia a AppCompatActivity y añadiremos la carpeta menú al directorio "res" y crearemos un nuevo archivo de recursos xml de tipo menú.

### Implementación de PolyLine y Polygon.
Para poder realizar la actividad de "dibujar" las líneas, marcamos un punto de partida con un click largo creando un marcador en la posición captada en el click.
Para hacer uso del objeto PolyLine, lo declaramos como estático y seguimos el patrón singleton para evitar que exista más de una sola instancia de este objeto.
El objeto PolyLine se va a ir construyendo y mostrando en pantalla con la captura de las coordenadas (LatLng) en cada click.
Cuando se decida realizar alguna de las opciones del menú, se procederá a crear el objeto Polygon y realizar los cálculos.
Se decide implementar una solución para que mientras exista un objeto Polygon no se pueda dibujar nada más en pantalla.

### Implementación del menú.
Los cálculos de área y perímetro, así como el borrado del objeto Polygon, se llaman desde tres entradas menuItem del menú de la barra de soporte.

![muestra menú y toast](https://i.imgur.com/KdWNNzk.png)


### Soluciones añadidas.
Para evitar una salida por Toast de números excesivamente grandes, se crean dos métodos de formateo de esa salida de información. De esta forma ganamos en legibilidad en superficies o perímetros bastos.

Tratamos posibles excepciones en clicks no controlados y subordinamos el uso de PolyLine a la no existencia de un Polygon en curso.

![m-km](https://i.imgur.com/6nlCxE8.png)
![m2-km2](https://i.imgur.com/mSE0mZ1.png)


